#!/bin/bash

#Output file path
output_file="/tmp/serverinfo.info"

# Print current date and time
echo "Date:" >> "$output_file"
date >> "$output_file"
echo >> "$output_file"

# Print last 10 users who logged in
echo "Last 10 users who logged in:" >> "$output_file"
last -n 10 >> "$output_file"
echo >> "$output_file"

# Print swap space
echo "Swap Space:" >> "$output_file"
free -h | grep Swap >> "$output_file"
echo >> "$output_file"

# Print kernel version
echo "Kernel Version:" >> "$output_file"
uname -r >> "$output_file"
echo >> "$output_file"

# Print IP address
echo "IP Address:" >> "$output_file"
hostname -I >> "$output_file"
echo >> "$output_file"

echo "Information gathered and saved to $output_file"

